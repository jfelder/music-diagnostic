#!/bin/bash

# https://stackoverflow.com/questions/4023830/how-to-compare-two-strings-in-dot-separated-version-format-in-bash
vercomp () {
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 1
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done
    return 0
}


# is Tracker is installed
if ! [ -x "$(command -v tracker)" ]; then
    echo "[ERROR] Tracker is not installed";
    exit 1;
else
    echo "[OK] Tracker is installed";
fi

# is tracker recent enough
TRACKER_VERSION="$(tracker --version | head -n 1 | cut -c9-13)"
TRACKER_MIN_VERSION='2.3.0'
vercomp $TRACKER_VERSION $TRACKER_MIN_VERSION
if [ $? -eq "2" ]; then
    echo "[ERRROR] Tracker version ($TRACKER_VERSION) is too old. Need at least $TRACKER_MIN_VERSION" ;
    exit 1;
else
    echo "[OK] Tracker is recent enough ($TRACKER_VERSION)";
fi

# is XDG_MUSIC_DIR set
MUSIC_DIR="$(xdg-user-dir MUSIC)"

if [ "$MUSIC_DIR" == "$(xdg-user-dir)" ]; then
    echo "[ERROR] XDG_MUSIC_DIR is not set";
    echo "Use this comment to set it:";
    echo "xdg-user-dirs-update --set MUSIC /path/to/music/collection"
    exit 1;
else
    echo "[OK] Music directory is set: $MUSIC_DIR";
fi

# is there some music in XDG_MUSIC_DIR
AUDIO_FILES_NR="$(find $MUSIC_DIR -type f \( -iname \*.mp3 -o -iname \*.flac -o -iname \*.ogg -o -iname \*.m4a \) | wc -l)"
if [ "$AUDIO_FILES_NR" -eq "0" ]; then
    echo "[WARNING] The music directory ($MUSIC_DIR) does not seem to contain any audio file";
else
    echo "[OK] $AUDIO_FILES_NR audio files found in $MUSIC_DIR";
fi

# has tracker started
TRACKER_RUNNING="$(tracker daemon -p | head -n 1 | cut -c1-1)"
if [ "$TRACKER_RUNNING" -eq "0" ]; then
    echo "[WARNING] Tracker has not started";
    echo "Start Tracker with this command:";
    echo "tracker daemon -s";
else
    echo "[OK] Tracker has started";
fi

# is tracker ready and is there some indexed content
read FILES_NR DIRS_NR < <(LANG=C tracker status | head -n 1 |awk '{ print $3 " " $5}');
MINER_FS_STATUS="$(LANG=C tracker daemon | grep -i 'file system' | tr -s ' ' | cut -d' ' -f 9-)";
MINER_FS_STATUS="${MINER_FS_STATUS%% }";

EXTRACTOR_STATUS="$(LANG=C tracker daemon | grep -i 'extractor' | tr -s ' ' |cut -d' ' -f 8-)";
EXTRACTOR_STATUS="${EXTRACTOR_STATUS%% }";
EXTRACTOR_OK="Not running or is a disabled plugin";

if [ "$FILES_NR" -eq "0" ] && [ "$TRACKER_RUNNING" -eq "0" ]; then
    echo "[ERROR] Tracker has not indexed any content and has not started";
    echo "Start Tracker with this command:";
    echo "tracker daemon -s";
    exit 1;
elif [ "$FILES_NR" -eq "0" ] && [ "$MINER_FS_STATUS" == "Idle" ]; then
    echo "[ERROR] No indexed content and miner-fs is not running"
    exit 1;
elif [ "$MINER_FS_STATUS" != "Idle" ] || \
     ( [ "$EXTRACTOR_STATUS" != "$EXTRACTOR_OK" ] && [ "$EXTRACTOR_STATUS" != "Idle" ] ) && \
     [ "$TRACKER_RUNNING" -ne "0" ]; then
    echo "[INFO] Tracker miners are active";
    echo "[INFO] Tracker miner-fs status: $MINER_FS_STATUS";
    echo "[INFO] Tracker extractor status: $EXTRACTOR_STATUS";
else
    echo "[OK] Tracker is ready: $FILES_NR files and $DIRS_NR direcories indexed";
fi

# is XDG_MUSIC_DIR indexed
MUSIC_INDEXED="$(tracker info -f $MUSIC_DIR | wc -l)";
if [ "$MUSIC_INDEXED" -lt "5" ]; then
    echo "[ERROR] Music directory is not indexed";
    echo "Run this command:";
    echo "tracker index -f $MUSIC_DIR";
    exit 1;
else
    echo "[OK] Music directory is indexed";
fi

# ha stracker indexed some songs
NB_SONGS="$(tracker sparql -q "SELECT COUNT(?a) WHERE {?a a nmm:MusicPiece}"  | tr -dc '0-9')"
if [ $NB_SONGS -eq "0" ]; then
    echo "[ERROR] Tracker has not found any song";
    exit 1;
else
    echo "[OK] Tracker has found $NB_SONGS songs";
fi
