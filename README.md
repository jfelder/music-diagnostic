# Introduction

Some scripts to help [GNOME Music](https://wiki.gnome.org/Apps/Music) development.

## Tracker diagnostic

[tracker-diagnostic.sh](tracker-diagnostic.sh) parses some [Tracker](https://wiki.gnome.org/Projects/Tracker) commands to check that it works correctly.

## Albums creation

### Introduction

[create-albums.py](create-albums.py) creates some albums (default number is `100`) in the `test-music-albums` directory. Each album has a random number of songs (between 2 and 13). These albums can be used in GNOME Music for testing purposes.

Each song comes from the [smcameron_medium-splash.ogg](files/smcameron_medium-splash.ogg) audio file. Some tags are automatically created: `title`, `album` and `artist`.

A covert art is also sometimes added. There are three possibilies:
 * An embedded image is inserted in the audio file
 * `album` and `artist` tags are set from a list of albums which a cover art on the [The AudioDB](https://theaudiodb.com) website. GNOME Music will be able to download the cover.
 * No cover

You need the [mutagen](https://github.com/quodlibet/mutagen) Python module to run the script. It is used to write the tags to the audio files.

### Usage

```sh
$ ./create-albums.py 250
```

### Testing in GNOME Music

You need to copy `test-music-albums` in your music directory and create a backup of your existing cover files:

```sh
$ cp -r test-music-albums ~/Music/
$ mv ~/.cache/media-art/ ~/.cache/media-art-backup
```

Once finished, restore your files:

```sh
$ rm -rf ~/Music/test-music-albums
$ mv ~/.cache/media-art-backup/ ~/.cache/media-art
```

# Credits

## Audio file

[medium_splash3.ogg](https://freesound.org/s/50785) by [smcameron](https://freesound.org/people/smcameron) is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0)

## Covers

 * [carrot updating or repairing](https://www.peppercarrot.com/0_sources/0ther/misc/low-res/2016-04-13_carrot-updating-or-repairing_by-David-Revoy.jpg) by [David Revoy](https://www.davidrevoy.com) is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0)
 * [fanfiction](https://www.peppercarrot.com/0_sources/0ther/misc/low-res/2019-05-22_fanfiction_by-David-Revoy.jpg) by [David Revoy](https://www.davidrevoy.com) is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0)
 * [Flight of Spring](https://www.peppercarrot.com/0_sources/0ther/artworks/low-res/2019-02-27_Flight-of-Spring_by-David-Revoy.jpg) by [David Revoy](https://www.davidrevoy.com) is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0)
 * [GNUess libreplanet livestream](https://www.peppercarrot.com/0_sources/0ther/misc/low-res/2020-03-14_GNUess-libreplanet-livestream_by-David-Revoy.jpg) by [David Revoy](https://www.davidrevoy.com) is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0)
 * [irc community](https://www.peppercarrot.com/0_sources/0ther/misc/low-res/2017-07-11_irc_community_by-David-Revoy.jpg) by [David Revoy](https://www.davidrevoy.com) is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0)
