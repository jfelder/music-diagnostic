#!/usr/bin/env python3

import argparse
import base64
import os
from random import randrange
import shutil

from mutagen.oggvorbis import OggVorbis
from mutagen.flac import Picture


ONLINE_ALBUMS = [
 {"artist": "M. Pokora", "title": "Mise à jour"},
 {"artist": "Madeleine Peyroux", "title": "Anthem"},
 {"artist": "Maggie Baugh", "title": "Only Good Things"},
 {"artist": "Magic", "title": "Skys The Limit"},
 {"artist": "Mägo de Oz", "title": "Hechizos, pócimas y brujería"},
 {"artist": "Maître Gims", "title": "Subliminal"},
 {"artist": "Malcolm McLaren", "title": "Paris"},
 {"artist": "Maki Goto", "title": "SWEET BLACK"},
 {"artist": "Malou", "title": "Puzzle"},
 {"artist": "Mandolin Orange", "title": "Blindfaller"},
 {"artist": "Mandy & Randy", "title": "Love for Eternity"},
 {"artist": "Marco Z", "title": "the ordinary life of marco z"}
]

COVERS_DIR = os.path.join("files", "covers")
NOISE = os.path.join("files", "smcameron_medium-splash.ogg")

MUSIC_DIR = "test-music-albums"

ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def create_images_covers():
    images_covers = []
    for cover_name in os.listdir(COVERS_DIR):
        cover_path = os.path.join(COVERS_DIR, cover_name)
        if not os.path.isfile(cover_path):
            continue

        with open(cover_path, "rb") as f_in:
            picture = Picture()
            picture.data = f_in.read()
            picture.type = 3
            picture.mime = "image/jpeg"

            encoded_data = base64.b64encode(picture.write())
            vcomment_value = encoded_data.decode("ascii")
            images_covers.append(vcomment_value)

    return images_covers


def create_albums(albums_nr, images_covers):
    online_album_idx = 0
    cover_idx = 0
    covers_max = 3
    for album_nr in range(1, albums_nr + 1):
        cover_type = randrange(0, covers_max)

        album_path = "album_{}".format(album_nr)
        album_dir = os.path.join(MUSIC_DIR, album_path)
        os.mkdir(album_dir)

        album_prefix = ALPHABET[randrange(0, len(ALPHABET))]
        album_title = album_prefix + " " + album_path.replace("_", " ")

        tags = {
            "artist": "smcameron",
            "album": album_title + " (no cover)",
            "picture": ""
        }

        if cover_type == 1:
            tags["album"] = album_title
            tags["picture"] = images_covers[cover_idx]
            cover_idx += 1
            if cover_idx >= len(images_covers):
                cover_idx = 0
        elif cover_type == 2:
            covers_info = ONLINE_ALBUMS[online_album_idx]
            tags["album"] = covers_info["title"]
            tags["artist"] = covers_info["artist"]
            online_album_idx += 1
            if online_album_idx >= len(ONLINE_ALBUMS):
                covers_max = 2

        for song_idx in range(1, randrange(3, 14)):
            song_title = "song_{}".format(song_idx)
            song_path = os.path.join(album_dir, song_title + ".ogg")
            shutil.copyfile(NOISE, song_path)

            audio_file = OggVorbis(song_path)
            audio_file["title"] = song_title.replace("_", " ")
            audio_file["album"] = tags["album"]
            audio_file["artist"] = tags["artist"]
            if tags["picture"]:
                audio_file["metadata_block_picture"] = tags["picture"]

            audio_file.save()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "albums_nr", nargs='?', type=int, default=100, help="number of albums")
    args = parser.parse_args()
    albums_nr = args.albums_nr

    print("Create {} albums".format(albums_nr))

    # Delete the previous MUSIC_DIR and create a new one
    if os.path.isdir(MUSIC_DIR):
        shutil.rmtree(MUSIC_DIR)

    os.mkdir(MUSIC_DIR)
    images_covers = create_images_covers()
    create_albums(albums_nr, images_covers)


if __name__ == "__main__":
    main()
